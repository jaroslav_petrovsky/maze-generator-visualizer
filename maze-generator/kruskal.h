/*
 * Date:    13.09.2016
 * Author:  Jaroslav Petrovsky
 *
 * Brief: Interface for Randomized Kruskal's algorithm
 * https://en.wikipedia.org/wiki/Maze_generation_algorithm#Randomized_Kruskal.27s_algorithm
 */

#ifndef KRUSKAL_H
#define KRUSKAL_H

// forward declaration
struct maze_grid;
struct tree;
struct edge;

/*
 * Apply kruskal's algorithm on maze grid do generate maze.
 * Function is allowed to modify given struct maze_grid
 */
int kruskal_generate(struct maze_grid* maze);


/*
 * Retrieve output maze information
 */
int kruskal_get_output(struct tree** maze_tree, struct edge*** edges, int* edges_number);

/*
 * Take care of all the resources algorithm allocated
 */
void kruskal_clean_up(void);

#endif // KRUSKAL_H
