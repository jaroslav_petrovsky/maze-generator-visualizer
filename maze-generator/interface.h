/*
 * Date:    13.09.2016
 * Author:  Jaroslav Petrovsky
 *
 * Brief: Common interface and helper functions for maze generation algorithms.
 * To support several algorithms, decision was made that binary tree is used to
 * represent generated maze and array (list,sequence,..) of edges is used as
 * data for animation. These edges must be in order (1..n) in which they were
 * deleted to clearly display how maze was generated.
 */

#ifndef INTERFACE_H
#define INTERFACE_H

#include <stdlib.h>

/*
 * maze data types
 */

struct cell {
    size_t row;
    size_t col;
    struct tree* tree;
};

struct edge {
    struct cell* from;
    struct cell* to;
    int weight;
};

struct maze_grid {
    size_t rows;
    size_t cols;
    // cells are arranged into one dimensional array, so proper access
    // functions must be used to get individual cells
    struct cell** cells;
    struct edge** edges;
    size_t edges_number;
};

/*
 * Linked-list structure to keep record of sets to which trees belong
 */
struct node {
    struct node* next;
    struct tree* tree;
};

/*
 * We use binary tree to indicate how maze cells are connected to create a
 * passage.To cover all edges we just have to consider 2 of the cell edges
 * (from total of 4), because the other 2 are (or will be) already the edges
 * of another cell.
 */
struct tree {
    struct tree* parent;
    struct tree* right; // connection through north edge
    struct tree* left;  // connection through west edge
    struct cell* cell;
    struct node* head;  // linked-list head
};

/******************************************************************************/

/*
 * Common helper functions
 */

/*
 * Allocate memory for and initialize all the cells and all the edges.
 * Returns MAZE_GENERATION_RESULT.
 */
int setup_maze_grid(struct maze_grid** grid, size_t rows, size_t cols);

/*
 * Release memory used by cells and edges in maze grid and set pointer to
 * maze_grid to null
 */
void release_maze_grid(struct maze_grid** grid);

/*
 * Allocate and initialize a new struct tree
 */
struct tree* create_tree(struct tree* root, struct tree* right, struct tree* left, struct cell* cell, struct node* head);

/*
 * Release memory held by a trees and set a pointer to a tree to null.
 */
void release_trees(struct node* head);

/*
 * Allocate and initialize a new struct edge
 */
struct edge* create_edge(struct cell* from, struct cell* to, int weight);

/*
 * Release memory held by an edge and set a pointer to an edge to null.
 */
void release_edge(struct edge** edge);

/*
 * Allocate and initialize a new struct cell
 */
struct cell* create_cell(size_t row, size_t col, struct tree* tree);

/*
 * Release memory held by a cell and set a pointer to a cell to null.
 */
void release_cell(struct cell** cell);

/*
 * Allocate and initialize a new struct node
 */
struct node* create_list_node(struct tree* tree);

/*
 * Release memory held by a node and set a pointer to a node to null.
 */
void release_list_node(struct node** node);

/*
 * Release memory held by a linked-list
 */
void release_list(struct node** head);

/*
 * Get index of a cell in struct maze_grid at (row, coll)
 */
int get_cell_index(struct maze_grid* grid, size_t row, size_t col);

/*
 * Get cell in struct maze_grid at (row, coll)
 */
struct cell* get_cell(struct maze_grid* grid, size_t row, size_t col);

#endif // INTERFACE_H
