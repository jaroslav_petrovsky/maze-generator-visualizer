/*
 * Date:    04.09.2016
 * Author:  Jaroslav Petrovsky
 *
 * Brief:   maze-generator main function. This program generates
 *          maze using chosen algorithm and outputs it acccording
 *          to command line options.
 */

#include "functions.h"
#include "interface.h"

int main (int argc, char** argv)
{
    int res;

    // parse command line options
    parse_options(argc, argv);

    res = get_errno();

    // process parsing errors
    if (res != EOK)
    {
        print_error();
        if (res == EHELP)
            return 0;
        else
            return -1;
    }

    generate_maze();

    res = get_errno();

    if (res != EOK)
    {
        print_error();
        return -1;
    }

    print_output();

    res = get_errno();

    if (res != EOK)
    {
        print_error();
        return -1;
    }

    return 0;
}

