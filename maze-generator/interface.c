/*
 * Date:    13.09.2016
 * Author:  Jaroslav Petrovsky
 *
 * Brief: Common helper functions implementation
 */

#include "interface.h"
#include "functions.h"

#include <stdlib.h>

// forward declarations for private functions
void init_cells(struct maze_grid* grid);
void init_edges(struct maze_grid* grid);
void release_cells(struct maze_grid* grid);
void release_edges(struct maze_grid* grid);

int setup_maze_grid(struct maze_grid** grid, size_t rows, size_t cols)
{
    *grid = (struct maze_grid*) malloc(sizeof(struct maze_grid));

    if (*grid == 0)
        return EALLOC;

    (*grid)->rows = rows;
    (*grid)->cols = cols;

    init_cells(*grid);

    if ((*grid)->cells == 0) {
        release_maze_grid(grid);
        return EALLOC;
    }

    init_edges(*grid);

    if ((*grid)->edges == 0) {
        release_maze_grid(grid);
        return EALLOC;
    }

    return EOK;
}

void release_maze_grid(struct maze_grid** grid)
{
    struct node* head = (*grid)->cells[0]->tree->head;
    release_trees(head);

    // pick arbitrary edge->cell->tree-> head as all heads should be the same
    if (head != 0)
        release_list(&head);

    if ((*grid)->cells != 0)
        release_cells(*grid);

    if ((*grid)->edges != 0)
        release_edges(*grid);

    free((void*) *grid);
    *grid = 0;
}

struct tree* create_tree(struct tree* root, struct tree* right, struct tree* left, struct cell* cell, struct node* head)
{
    struct tree* tree = (struct tree*) malloc(sizeof(struct tree));

    if (tree == 0)
        return 0;

    tree->parent = 0;
    tree->right = right;
    tree->left = left;
    tree->cell = cell;
    tree->head = head;

    return tree;
}

void release_trees(struct node* head)
{
    struct node* tmp = head;
    while (tmp != 0) {
        free(tmp->tree);
        tmp->tree = 0;
        tmp = tmp->next;
    }
}

struct edge* create_edge(struct cell* from, struct cell* to, int weight)
{
    struct edge* edge = (struct edge*) malloc(sizeof(struct edge));

    if (edge == 0)
        return 0;

    edge->from = from;
    edge->to = to;
    edge->weight = weight;

    return edge;
}

void release_edge(struct edge** edge)
{
    free((void*) *edge);
    *edge = 0;
}

struct cell* create_cell(size_t row, size_t col, struct tree* tree)
{
    struct cell* cell = (struct cell*) malloc(sizeof(struct cell));

    if (cell == 0)
        return 0;

    cell->row = row;
    cell->col = col;
    cell->tree = tree;

    return cell;
}

void release_cell(struct cell** cell)
{
    free((void*) *cell);
    *cell = 0;
}

int get_cell_index(struct maze_grid* grid, size_t row, size_t col)
{
    return (row * grid->cols + col);
}

struct cell* get_cell(struct maze_grid* grid, size_t row, size_t col)
{
    return grid->cells[get_cell_index(grid, row, col)];
}

/*
 * Allocate needed memory for maze cells.
 * Setup each cell with x,y coordinates and empty tree
 */
void init_cells(struct maze_grid* grid)
{
    grid->cells = (struct cell**) malloc(grid->rows * grid->cols * sizeof(struct cell*));

    if (grid->cells == 0)
        return;

    size_t i, j;
    for(i = 0; i < grid->rows; ++i) {
        for(j = 0; j < grid->cols; ++j) {
            struct tree* tree = create_tree(0, 0, 0, 0, 0);

            if (tree == 0) {
                release_cells(grid);
                return;
            }

            grid->cells[get_cell_index(grid, i, j)] = create_cell(i, j, tree);
            tree->cell = grid->cells[get_cell_index(grid, i, j)];

            tree->head = create_list_node(tree);
        }
    }
}

/*
 * Allocate needed memory for maze edges.
 * Setup each cell with cell and direction data.
 */
void init_edges(struct maze_grid* grid)
{
    // each cell has edge in 2 directions, 2 * thus settings.rows_number * settings.cols_number
    // Top rows and right-most column has edge in only one direction, thus subtract settings.rows_number + settings.cols_number
    // NE corner has no edge
    grid->edges_number = (2 * grid->rows * grid->cols) - (grid->rows + grid->cols);
    grid->edges = (struct edge**) malloc(grid->edges_number * sizeof(struct edge*));

    if (grid->edges == 0)
        return;

    // build the collection of edges.
    // Edges are "north" and "west" sides of cell,
    // if index is greater than 0 (no N edged in first row and no W edges in first column)
    // direction N/W
    size_t row, col, i = 0;
    for (row = 0; row < grid->rows; ++row) {
        for (col = 0; col < grid->cols; ++col) {
            if ( col > 0 ) {
                grid->edges[i++] = create_edge(
                            get_cell(grid, row, col),
                            get_cell(grid, row, col - 1),
                            1); // west

                if (grid->edges[i - 1] == 0) {
                    release_edges(grid);
                    return;
                }
            }


            if ( row > 0 ) {
                grid->edges[i++] = create_edge(
                            get_cell(grid, row, col),
                            get_cell(grid, row - 1, col),
                            1); // north

                if (grid->edges[i - 1] == 0) {
                    release_edges(grid);
                    return;
                }
            }
        }
    }
}

void release_cells(struct maze_grid* grid)
{
    if (grid->cells != 0) {
        size_t size = grid->rows * grid->cols;

        // first free all array members
        while(--size > 0)
            free(grid->cells[size]);

        // free the last element
        free(grid->cells[0]);
    }

    // free an array itself
    free(grid->cells);
    grid->cells = 0;
}

void release_edges(struct maze_grid* grid)
{
    if (grid->edges != 0) {
        size_t size = grid->edges_number;

        // first free all array members
        while(--size > 0)
            free(grid->edges[size]);

        // free the last element
        free(grid->edges[0]);
    }

    // free an array itself
    free(grid->edges);
    grid->edges = 0;
}

struct node* create_list_node(struct tree* tree)
{
    struct node* node = (struct node*) malloc(sizeof(struct node));

    if (node == 0)
        return 0;

    node->tree = tree;
    node->next = 0;

    return node;
}

void release_list(struct node** head)
{
    struct node* p_free = (*head);
    struct node* next;

    while (p_free->next != 0) {
        next = p_free->next;

        free((void*) p_free);

        p_free = next;
    }

    free((void*) p_free);
    p_free = 0;

    *head = 0;
}
