/*
 * Date:    04.09.2016
 * Author:  Jaroslav Petrovsky
 *
 * Brief:   maze-generator logic split into functions
 */

#include "functions.h"
#include "kruskal.h"
#include "interface.h"

#include <getopt.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

// max length of 255 characters according to
// https://en.wikipedia.org/wiki/Comparison_of_file_systems#Limits
#define MAX_PATHNAME_LENGTH 255

// 20 is magic number (could be also 15,50 or 76)
#define MAX_ALGORITHM_NAME_LENGTH 20

// default options
#define DEFAULT_ANIMATION_FILENAME  "animation.txt"
#define DEFAULT_TREE_FILENAME       "tree.txt"
#define DEFAULT_ALGORITHM           0
#define DEFAULT_ANIMATE             0
#define DEFAULT_TREE                1
#define DEFAULT_ROWS_NUMBER         100
#define DEFAULT_COLS_NUMBER         100

enum ALGORITHM {
    KRUSKAL
};

// forward declaration of private functions
char* strncpynt (char *dest, const char *src, size_t dest_size);
void print_tree_info(void);
void print_animation_info(void);

// Error number according to READ_OPTIONS_RESULT
// underscore at the beggining to distinguisg from errno in <errno.h>
static int _errno = EOK;

// application settings
static struct settings {
    // algorithm for maze generation
    short algorithm;

    // Flag, true = create file containing animation info for maze-visalizer
    int animate;

    // Flag, true = create file containing maze tree for maze-visualizer
    int tree;

    // maze size
    size_t rows_number;
    size_t cols_number;

    // Data filenames
    char animation_path[MAX_PATHNAME_LENGTH];
    char tree_path[MAX_PATHNAME_LENGTH];
} settings = {
    DEFAULT_ALGORITHM,
    DEFAULT_ANIMATE,
    DEFAULT_TREE,
    DEFAULT_ROWS_NUMBER,
    DEFAULT_COLS_NUMBER,
    DEFAULT_ANIMATION_FILENAME,
    DEFAULT_TREE_FILENAME
};

static struct maze_grid* grid;

// output information
static struct tree* maze;
static struct edge** edges;
static int edges_number;

void parse_options(int argc, char** argv)
{
    // structure defines command line options
    static struct option long_options[] =
    {
        // print help
        {"help",            no_argument,        0, 'h'},
        // choose maze generation algorithm; default - DEFAULT_ALGORITHM
        {"algorithm",       required_argument,  0, 'a'},
        // output maze generation animation info
        // default - DEFAULT_ANIMATE
        {"animate",         required_argument,  0, 'n'},
        // filepath for maze generation animation info
        {"animation-path",  required_argument,  0, 'p'},
        // output generated maze tree
        // default - DEFAULT_TREE
        {"tree",            required_argument,  0, 't'},
        // filepath for generated maze tree
        {"tree-path",       required_argument,  0, 'f'},
        {"rows",            required_argument,  0, 'r'},
        {"cols",            required_argument,  0, 'c'},
        {0, 0, 0, 0}
    };

    // iterate through all the options until there is no option left
    int option = 0;
    int option_index = 0;
    while ((
        option = getopt_long (
            argc,
            argv,
            "ha:n:p:m:f:r:c:",
            long_options,
            &option_index)
        ) != -1)
    {
        switch (option)
        {
            case 'h': _errno = EHELP;
                break;
            case 'a':
                if (!strcmp("kruskal", optarg))
                    settings.algorithm = KRUSKAL;
                break;
            case 'n': settings.animate = atoi(optarg);
                break;
            case 'p': strncpynt(settings.animation_path, optarg, MAX_PATHNAME_LENGTH);
                break;
            case 't': settings.tree = atoi(optarg);
                break;
            case 'f': strncpynt(settings.tree_path, optarg, MAX_PATHNAME_LENGTH);
                break;
            case 'r': settings.rows_number = atoi(optarg);
                break;
            case 'c': settings.cols_number = atoi(optarg);
                break;
            default:
                _errno = EHELP;
        }

        if (_errno == EHELP)
            return;
    }

    // check options for validity
    if ( (settings.animate != 0 && settings.animate != 1) ||
         (settings.tree != 0 && settings.tree != 1) )
        _errno = EARG;


    return;
}

void print_help (void)
{
    printf("maze-generator is a program to generate maze using chosen algorithm\n");
    printf("\n");
    printf("\t--help -h \t\t- prints this help\n");
    printf("\t--algorithm -a \t\t- string determining algorithm for maze generation\n");
    printf("\t\t\t\t \"%s\" (default)\n", DEFAULT_ALGORITHM);
    printf("\t--animate -n \t\t- 0/1 value indicating whether to generate animation information for maze-visualizer (default: %d)\n", DEFAULT_ANIMATE);
    printf("\t--animation-path -p \t- filename for animation info (default: %s)\n", DEFAULT_ANIMATION_FILENAME);
    printf("\t--tree -t \t\t- 0/1 value indicating whether to generate maze tree information (default: %d)\n", DEFAULT_TREE);
    printf("\t--tree-path -f \t\t- filename for maze tree info (default: %s)\n", DEFAULT_TREE_FILENAME);
    printf("\t--rows -r \t\t- number of maze rows (default: %d)\n", DEFAULT_ROWS_NUMBER);
    printf("\t--cols -c \t\t- number of maze columns (default: %d)\n", DEFAULT_COLS_NUMBER);
}

void print_error(void)
{
    switch (_errno)
    {
        case EHELP: print_help();
            break;
        case EARG: printf("Invalid argument(s)!\n");
            break;
        case EALLOC: printf("Memory allocation error!\n");
            break;
        case EPREC: printf("Maze generation precondition failed!\n");
            break;
        case EOUTPUT: printf("Error when reading maze generation output!\n");
            break;
        case ETREEF: printf("Error opening output maze tree file!\n");
            break;
        case EANIMF: printf("Error opening output animation info file!\n");
            break;
        default: print_help();
    }
}

int get_errno(void)
{
    return _errno;
}

void generate_maze(void)
{
    _errno = setup_maze_grid(&grid, settings.rows_number, settings.cols_number);

    if (_errno != EOK)
        return;

    // functions pointers declarations
    int  (*generate)    (struct maze_grid* maze);
    int  (*get_output)  (struct tree** maze_tree, struct edge*** edges, int* edges_number);
    void (*clean_up)    (void);

    // function pointers definition according to algorithn
    switch (settings.algorithm) {
        case KRUSKAL:
            generate    = &kruskal_generate;
            get_output  = &kruskal_get_output;
            clean_up    = &kruskal_clean_up;
        break;

    }

    _errno = generate(grid);

    if (_errno != EOK) {
        clean_up();
        return;
    }

    _errno = get_output(&maze, &edges, &edges_number);

    clean_up();
}

void print_output(void)
{
    if (settings.tree == 1)
        print_tree_info();

    if (settings.animate == 1)
        print_animation_info();

    // no longer needed
    release_maze_grid(&grid);
}

void print_tree_info(void)
{
//    FILE* tree_file = fopen(settings.tree_path, "w");

//    if (tree_file == 0) {
//        _errno = ETREEF;
//        return;
//    }
}

void print_animation_info(void)
{
    FILE* anim_file = fopen(settings.animation_path, "w");

    if (anim_file == 0) {
        _errno = EANIMF;
        return;
    }

    fprintf(anim_file, "%d,%d\n", settings.rows_number, settings.cols_number);

    struct cell* from = 0;
    struct cell* to = 0;
    int i;
    for (i = 0; i < edges_number; ++i) {
        from = edges[i]->from;
        to = edges[i]->to;


        fprintf(anim_file,
                "[%d,%d]-[%d,%d]\n",
                from->row,
                from->col,
                to->row,
                to->col);
    }

    fclose(anim_file);
}

/*
 * Functions does the same as strncpy but adds null character at the
 * end of the dest to protect from buffer overrun (nt - null terminated),
 * as this is not guaranteed from strncpy alone.
 */
char* strncpynt (char *dest, const char *src, size_t dest_size)
{
    strncpy(dest, src, dest_size);
    dest[dest_size - 1] = '\0';
}
