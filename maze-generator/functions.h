/*
 * Date:    04.09.2016
 * Author:  Jaroslav Petrovsky
 *
 * Brief:   Header file containing prototypes for all public
 *          functions containing maze-genrator logic.
 */

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

/*
 * Error states
 */

#define EOK     0   // OK
#define EHELP   1   // print help
#define EARG    2   // invalid argument
#define EALLOC  3   // allocation error
#define EPREC   4   // precondition failed
#define EOUTPUT 5   // generation output error
#define ETREEF  6   // error when manipulating tree file
#define EANIMF  7   // error when manipulating animation file

/*
 * Parse command line oprions
 *
 * params:
 *  int argc    - argc from main
 *  char** argv - argv from main
 * return:
 *  int - PARSE_OPTIONS_RESULT
 */
void parse_options (int argc, char** argv);

/*
 * Print help (--help/-h) for using the program
 */
void print_help(void);

/*
 * Print error according to current status of errno variable
 */
void print_error(void);

/*
 * Errno getter. Errno indicates result status of operations.
 * This is application specific errno different from errno.h
 */
int get_errno(void);

/*
 * Generates maze according to command line parameters and outputs maze and/or
 * animation information into output files. Populates errno with the result.
 */
void generate_maze(void);

/*
 * Print maze and animation info into output files
 */
void print_output(void);

#endif // FUNCTIONS_H
