/*
 * Date:    13.09.2016
 * Author:  Jaroslav Petrovsky
 *
 * Brief: Implementation of randomized Kruskal's algorithm
 */

#include "kruskal.h"
#include "interface.h"
#include "functions.h"

#include <stdlib.h>

// output edges for animation info
static struct edge** output_edges = 0;
static int output_edges_number = 0;
static int output_edges_capacity = 100;

// forward declarations
void shuffle(struct edge** edges, size_t size);
void swap (struct edge* a, struct edge *b);
void connect_lists(struct node* head, struct node* child);
void connect_trees(struct tree* parent, struct tree* child);
int add_output_edge(struct edge* edge);
void clean_up(void);

int kruskal_generate(struct maze_grid* maze)
{
    if (maze->edges == 0)
        return EPREC;

    shuffle(maze->edges, maze->edges_number);

    size_t i;
    for(i = 0; i < maze->edges_number; ++i) {
        struct cell* from = maze->edges[i]->from;
        struct cell* to = maze->edges[i]->to;

        if(from->tree->head == to->tree->head)
            continue;

        connect_lists(from->tree->head, to->tree->head);
        connect_trees(from->tree, to->tree);

        if (add_output_edge(maze->edges[i]) != EOK)
            return EALLOC;
    }

    return EOK;
}

int kruskal_get_output(struct tree** maze_tree, struct edge*** edges, int* edges_number)
{
    *edges =
            (struct edge**) malloc(output_edges_number * sizeof(struct edge*));

    if (*edges == 0)
        return EOUTPUT;

    *edges_number = output_edges_number;

    int i;
    for(i = 0; i < output_edges_number; ++i)
        (*edges)[i] = output_edges[i];

    return EOK;
}

void kruskal_clean_up(void)
{
    // edges inside array will be deallocated with other struct maze_grid data
    free (output_edges);
}

// A utility function to shuffle an array
// http://benpfaff.org/writings/clc/shuffle.html
void shuffle(struct edge** edges, size_t size)
{
    if (size > 1) {
        size_t i;

        // Use a different seed value so that we don't get same
        // result each time we run this program
        srand(time(NULL));

        for (i = 0; i < size - 1; ++i) {
            size_t j = i + rand() / (RAND_MAX / (size - i) + 1);
            swap(edges[i], edges[j]);
        }
    }
}

// A utility function to swap two edges
void swap (struct edge* a, struct edge* b)
{
    struct edge tmp = *a;
    *a = *b;
    *b = tmp;
}

void connect_lists(struct node* head, struct node* child)
{
    // find last element of head list
    struct node* tmp = head;
    while (tmp->next != 0) {
        tmp = tmp->next;
    }

    // append child list
    tmp->next = child;

    // set head for child list
    while (tmp->next != 0) {
        tmp = tmp->next;
        tmp->tree->head = head;
    }
}

// Determine whether child tree should be placed into left or right leave
void connect_trees(struct tree* parent, struct tree* child)
{
    // according to struct tree definition  right is north edge and left is west
    struct cell* from = parent->cell;
    struct cell* to = child->cell;

    // row is same = west/left
    if (from->row == to->row)
        parent->left = child;
    // col is same = north/right
    else if (from->col == to->col)
        parent->right = child;

    child->parent = parent;
}

int add_output_edge(struct edge* edge)
{
    // setup array if first ever call
    if (output_edges == 0)
        output_edges =
                (struct edge**) malloc(output_edges_capacity * sizeof(struct edge*));

    if (output_edges == 0)
        return EALLOC;

    // realloc memory if needed
    if(output_edges_number + 1 > output_edges_capacity) {
        output_edges_capacity *= 2;

        struct edge** tmp =
                (struct edge**) realloc(output_edges, output_edges_capacity * sizeof(struct edge*));

        if (tmp == 0) {
            int i;
            for(i = 0; i < output_edges_number; ++i)
                release_edge(&output_edges[i]);

            free(output_edges);
            return EALLOC;
        }

        output_edges = tmp;

    }

    // output_edges_number is not +1 yet, so this index is OK
    // and postincrement will do its job then
    output_edges[output_edges_number++] = edge;

    return EOK;
}
