# README #

Redgate (http://www.red-gate.com/) first ever coding challenge solution.

"The challenge? To write a program to generate a maze. Preferably an amazing maze. You can use any language you like, from Python to SQL to C#, and any technology you like.

The only constraint is that we should be able to see what youve done and just as importantly how youve done it."

### Intro ###

When I was thinking about how to stucture my program I had first point of Unix philosophy (https://en.wikipedia.org/wiki/Unix_philosophy) on my mind so I've seen maze generation and maze visualization as two separate tasks so I decided to implement it as two separate programs. Although separate, self-standing programs, maze-visualizer is dependent on maze-generation. maze-generator is pure C command line utility which outputs result maze and maze animation information into files which are then processed by maze-visualizer. maze-visualizer is C++ & QT application.

### How do I get set up? ###

Both applications use cmake.

#### maze-generator ####

* c + standard library
* use --help parameter to print usage information

#### maze-visualizer ####

* c++ + qt version 5.4.1

So basically, if you have C++STD and QT libs installed, just clone the repo, go into maze-generator and/or maze-visualizer folder, run cmake to generate Makefile and then go into folder with generated Makefile and run make.

### About ###

#### maze-generator ####

Command line tools which generates maze of size given by input parameters.

##### Algorithms #####

The only supported algorithm is randomized kruskal's right now, but any given algorithm can be implemented by just supporting these three functions:

```
#!c++
    int  (*generate)    (struct maze_grid* maze); // generate using data from maze
    int  (*get_output)  (struct tree** maze_tree, struct edge*** edges, int* edges_number); // output generated maze
    void (*clean_up)    (void); // clean up data allocated by generation
```
##### Output #####

As generation visualization is separated from generation itself and it can be run without re-generating the maze I had to find a unique, algorithm independent way to output the generated maze. After examining a few algorithms I found out, that each maze can be described by its edges. Output animation file therefore contains list of edges in form of [fromCellRow, fromCellCol]-[toCellRow, toCellCol]. Let me explain this, please. It is not my idea, I understood it from the code of kruskal's algorithm implementations, when I was writing my one. Every edge in a maze can be described as some cell's north or west edge. Some cells have one, some have none, but the most of them have both. So by determining the direction (north or west) from which you move from the cell, you can determine the to cell. So the output contains number of rows and cols on the first line and then one "edge" in fromCell-toCell format per line.
Part of the output information is a tree structure describing the generated three without any animation information. This information is currently not used, as I thought that it would be needed for a maze-visualizer to display the whole maze quickly, but the reality showed that animation information in form of edges is enough. Also the "protocol" for outputting this three into a file is not developed.

##### Performance #####

Current code is not performance tuned, as mazes of "normal" size (100x100) take no time to generate, bigger ones (200x200 and 300x300) took a few seconds (around 2 and 20) on my machine and the biggest one (500x500) I generated took around 4 minutes.

#### maze-visualizer ####

QT GUI for displaying the maze and animation of maze. It has simple and intuitive interface to control maze show/hide and animation. The input for this application is an animation output file of maze-generator.

##### Performance #####

Mazes of decent size (e.g. 100x100) show no delays in drawing and input processing. The bigger the maze the bigger area to draw and this could lead to slowdown of input processing. The issue is that QT engine as it is takes too long to redraw the whole scene every 100ms. Solution is to cache maze into pixmap and redraw only cells and edges that are changed. This solution was tested and is working, however is not implemented as it was not necessary yet.