/*
 * Date:    26.09.2016
 * Author:  Jaroslav Petrovsky
 *
 * Brief:   Widget which displays maze grid animation.
 */

#ifndef MAZEGRID_H
#define MAZEGRID_H

#include "MazeData.h"

#include <QWidget>
#include <QSharedPointer>
#include <QMutex>
#include <QTimer>

class MazeGrid : public QWidget
{
    Q_OBJECT

public:
    enum class DrawingState {
        HIDDEN,
        VISIBLE,
        ANIMATING
    };

    /*
     * Edge class
     */
    class Edge : public MazeAnimationData::Edge
    {
    public:
        Edge(QSharedPointer<MazeAnimationData::Cell> from,
             QSharedPointer<MazeAnimationData::Cell> to,
             MazeGrid::DrawingState drawingState)
            : MazeAnimationData::Edge(from,to), m_drawingState(drawingState)
        {}
        ~Edge() = default;

        inline MazeGrid::DrawingState GetDrawingState() const { return m_drawingState; }

        inline void SetDrawingState(MazeGrid::DrawingState drawingState) { m_drawingState = drawingState; }

    private:
        MazeGrid::DrawingState m_drawingState;
    };

    /*
     * Cell class
     */
    class Cell : public MazeAnimationData::Cell
    {
    public:
        Cell(unsigned int row,
             unsigned int col,
             QSharedPointer<MazeGrid::Edge> northEdge,
             QSharedPointer<MazeGrid::Edge> westEdge,
             MazeGrid::DrawingState drawingState)
            : MazeAnimationData::Cell(row, col),
              m_northEdge(northEdge),
              m_westEdge(westEdge),
              m_drawingState(drawingState)
        {}
        ~Cell() = default;

        inline MazeGrid::DrawingState           GetDrawingState()   const { return m_drawingState; }
        inline QSharedPointer<MazeGrid::Edge>   GetNorthEdge()      const { return m_northEdge; }
        inline QSharedPointer<MazeGrid::Edge>   GetWestEdge()       const { return m_westEdge; }

        inline void SetNorthEdge(QSharedPointer<MazeGrid::Edge> northEdge)  { m_northEdge = northEdge; }
        inline void SetWestEdge(QSharedPointer<MazeGrid::Edge> westEdge)    { m_westEdge = westEdge; }

        inline void SetDrawingState(MazeGrid::DrawingState drawingState) { m_drawingState = drawingState; }

    private:
        // all grid edges can be adressed as nort or west edge of some cell
        QSharedPointer<MazeGrid::Edge> m_northEdge;
        QSharedPointer<MazeGrid::Edge> m_westEdge;

        MazeGrid::DrawingState m_drawingState;
    };

    explicit MazeGrid(QWidget* parentW = nullptr);
    ~MazeGrid() = default;

    void SetMazeAnimationData(QSharedPointer<MazeAnimationData> mazeAnimationData);

signals:
    void AnimationDone();
    void ProgressUpdated(int progress);

public slots:
    void ShowMaze();
    void ResetMaze();
    void PlayButtonClicked(bool checked);
    void SetPlaySpeed(int playSpeed);

protected:
    void paintEvent(QPaintEvent* pEvent);
    void timerEvent(QTimerEvent* tEvent);

private:
    void InitMazeGridData();
    void InitCells();
    void InitEdges();
    QSharedPointer<MazeGrid::Edge> CreateEdge(
            QSharedPointer<MazeGrid::Cell> fromCell,
            QSharedPointer<MazeGrid::Cell> toCell);

    QSharedPointer<MazeGrid::Cell> GetCell(unsigned int row, unsigned int col);
    unsigned int GetCellIndex(unsigned int row, unsigned int col);

    void ComputeDimensions();

    void DrawEdges(QPainter& painter);
    void DrawCells(QPainter& painter);
    void DrawBorder(QPainter& painter);

    void DrawEdge(
            size_t fromX,
            size_t fromY,
            size_t toX,
            size_t toY,
            DrawingState drawingState,
            QPainter& painter);
    void DrawCell(
            size_t fromX,
            size_t fromY,
            size_t cellWidth,
            size_t cellHeight,
            MazeGrid::DrawingState drawingState,
            QPainter& painter);

    bool CanResizeToFit();
    void ResizeToFit();

    void ChangeMazeState(MazeGrid::DrawingState state);

private slots:
    void Animate();

private:
    QSharedPointer<MazeAnimationData> m_mazeAnimationData;
    // grid data
    QVector<QSharedPointer<MazeGrid::Cell>> m_cells;
    QVector<QSharedPointer<MazeGrid::Edge>> m_edges;
    QMutex m_drawingMutex;
    bool m_isPlaying;
    QTimer m_playTimer;

    // temp settings - will change when settings panel will be implemented
    size_t m_minCellSize;
    size_t m_currentCellSize;
    size_t m_edgeSize;
    size_t m_borderSize;
    size_t m_margin;
    size_t m_originX;
    size_t m_originY;
    size_t m_mazeWidth;
    size_t m_mazeHeight;
    QColor m_hiddenCellColor;
    QColor m_visibleCellColor;
    QColor m_animatingCellColor;
    QColor m_edgeColor;
    QColor m_borderColor;

    int m_playSpeed;
    int m_progress;

    // animation data
    int m_edgeIndex;
    int m_animationStep;
    QSharedPointer<MazeGrid::Cell> m_currentFromCell;
    QSharedPointer<MazeGrid::Cell> m_currentToCell;
};

#endif // MAZEGRID_H
