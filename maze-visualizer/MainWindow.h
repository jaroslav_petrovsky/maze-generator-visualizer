/*
 * Date:    19.09.2016
 * Author:  Jaroslav Petrovsky
 *
 * Brief:   Application main window holds other widgets (maze & settings) and
 * process communication between them.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
    class MainWindowForm;
}

class MazeAnimationData;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();
    ~MainWindow();

    // set MazeAnimationData into MazeGrid
    void SetMazeAnimationData(QSharedPointer<MazeAnimationData> mazeAnimationData);

public slots:
    void UpdateProgress(int progress);

protected:
    bool event(QEvent *ev);

private slots:
    void PlayButtonClicked(bool checked);
    void ShowMazeClicked();
    void ResetMazeClicked();
    void ProcessAnimationDone();

private:
    Ui::MainWindowForm* m_gui;

    bool m_isMazeVisible;
};

#endif // MAINWINDOW_H
