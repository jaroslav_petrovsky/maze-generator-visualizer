/*
 * Date:    19.09.2016
 * Author:  Jaroslav Petrovsky
 *
 * Brief:   Application main window holds other widgets (maze & settings) and
 * process communication between them.
 */

#include "ui_MainWindowForm.h"

#include "MainWindow.h"

MainWindow::MainWindow()
    : QMainWindow(), m_gui(new Ui::MainWindowForm), m_isMazeVisible(false)
{
    m_gui->setupUi(this);

    QObject::connect(m_gui->showMazeButton, SIGNAL(clicked()), m_gui->maze, SLOT(ShowMaze()));
    QObject::connect(m_gui->resetPushButton, SIGNAL(clicked()), m_gui->maze, SLOT(ResetMaze()));

    QObject::connect(m_gui->playPushButton, SIGNAL(clicked(bool)), this, SLOT(PlayButtonClicked(bool)));
    QObject::connect(m_gui->showMazeButton, SIGNAL(clicked()), this, SLOT(ShowMazeClicked()));
    QObject::connect(m_gui->resetPushButton, SIGNAL(clicked()), this, SLOT(ResetMazeClicked()));

    QObject::connect(m_gui->playPushButton, SIGNAL(clicked(bool)), m_gui->maze, SLOT(PlayButtonClicked(bool)));
    QObject::connect(m_gui->playSpeed, SIGNAL(sliderMoved(int)), m_gui->maze, SLOT(SetPlaySpeed(int)));

    QObject::connect(m_gui->maze, SIGNAL(AnimationDone()), this, SLOT(ProcessAnimationDone()));
    QObject::connect(m_gui->maze, SIGNAL(ProgressUpdated(int)), this, SLOT(UpdateProgress(int)));

    m_gui->maze->SetPlaySpeed(m_gui->playSpeed->value());
    m_gui->progressBar->hide();
}

MainWindow::~MainWindow()
{
    delete m_gui;
}

void MainWindow::SetMazeAnimationData(QSharedPointer<MazeAnimationData> mazeAnimationData)
{
    m_gui->maze->SetMazeAnimationData(mazeAnimationData);
}

bool MainWindow::event(QEvent *ev) {
    if(ev->type() == QEvent::LayoutRequest) {
        setFixedSize(sizeHint());
    }
    return QMainWindow::event(ev);
}

void MainWindow::PlayButtonClicked(bool checked)
{
    m_gui->showMazeButton->setEnabled(!checked);
    m_gui->resetPushButton->setEnabled(!checked);

    if (checked == true)
        m_gui->playPushButton->setText("Pause");
    else if (checked == false)
        m_gui->playPushButton->setText("Play");
}

void MainWindow::ShowMazeClicked()
{
    if (m_isMazeVisible)
        return;

    m_isMazeVisible = true;

    m_gui->playPushButton->setEnabled(!m_isMazeVisible);
    m_gui->showMazeButton->setEnabled(!m_isMazeVisible);
    m_gui->resetPushButton->setEnabled(m_isMazeVisible);
    m_gui->playSpeed->setEnabled(!m_isMazeVisible);
}

void MainWindow::ResetMazeClicked()
{
    if (!m_isMazeVisible)
        return;

    m_isMazeVisible = false;

    m_gui->playPushButton->setEnabled(!m_isMazeVisible);
    m_gui->showMazeButton->setEnabled(!m_isMazeVisible);
    m_gui->resetPushButton->setEnabled(m_isMazeVisible);
    m_gui->playSpeed->setEnabled(!m_isMazeVisible);

    m_gui->progressBar->setValue(0);
    m_gui->progressBar->hide();
}

void MainWindow::ProcessAnimationDone()
{
    m_isMazeVisible = true;

    m_gui->playPushButton->setEnabled(!m_isMazeVisible);
    m_gui->playPushButton->setChecked(!m_isMazeVisible);
    m_gui->playPushButton->setText("Play");

    m_gui->showMazeButton->setEnabled(!m_isMazeVisible);
    m_gui->resetPushButton->setEnabled(m_isMazeVisible);
    m_gui->playSpeed->setEnabled(!m_isMazeVisible);

    m_gui->progressBar->setValue(0);
    m_gui->progressBar->hide();
}

void MainWindow::UpdateProgress(int progress)
{
    if (!m_gui->progressBar->isVisible())
        m_gui->progressBar->show();

    m_gui->progressBar->setValue(progress);

    if (m_gui->progressBar->value() == 100)
        m_gui->progressBar->hide();
}
