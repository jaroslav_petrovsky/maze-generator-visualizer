/*
 * Date:    19.09.2016
 * Author:  Jaroslav Petrovsky
 *
 * Brief:   MazeVisualizer is application object which intializes main window
 * in which all events and processing happens.
 */

#include "MazeVisualizer.h"

#include "MainWindow.h"

#include <QObject>
#include <QFileDialog>
#include <QMessageBox>
#include <QApplication>
#include <QTextStream>

MazeVisualizer::MazeVisualizer()
    : QObject(),
      m_mainWindow(new MainWindow()),
      m_mazeAnimationData(nullptr),
      m_isOK(false)
{
    QObject::connect(this, SIGNAL(ProgressUpdated(int)), m_mainWindow.data(), SLOT(UpdateProgress(int)));

    // show main window
    m_mainWindow->show();

    // request for file until one is given
    QString fileName;
    fileName = QFileDialog::getOpenFileName(m_mainWindow.data(), "Open Animation Data File");

    if (fileName == "")
        return;

    m_mazeAnimationData = ParseInputFile(fileName);

    // error parsing file
    if (m_mazeAnimationData == nullptr) {
        QMessageBox messageBox;
        messageBox.critical(
                    m_mainWindow.data(),
                    "Parse Error",
                    "An error has occured when parsing input file!");

        return;
    }

    m_mainWindow->SetMazeAnimationData(m_mazeAnimationData);

    m_isOK = true;
}

// cell format: "[row,col]"
QSharedPointer<MazeAnimationData::Cell> MazeVisualizer::ProcessCell(const QString& cellInfo)
{
    if (cellInfo[0] != '[' && cellInfo[cellInfo.size() - 1] != ']')
        return QSharedPointer<MazeAnimationData::Cell>();

    QStringList coords = cellInfo.split(",");

    if (coords.size() != 2)
        return QSharedPointer<MazeAnimationData::Cell>();

    int row = -1;
    int col = -1;
    bool ok;

    coords[0].remove('[');
    row = coords[0].toInt(&ok);

    if (ok == false)
        return QSharedPointer<MazeAnimationData::Cell>();

    coords[1].remove(']');
    col = coords[1].toInt(&ok);
    if (ok == false)
        return QSharedPointer<MazeAnimationData::Cell>();

    return QSharedPointer<MazeAnimationData::Cell>(
                new MazeAnimationData::Cell((unsigned int) row, (unsigned int) col));
}

// edges are in format of "[fromRow,fromCol]-[toRow,toCol]"
QSharedPointer<MazeAnimationData::Edge> MazeVisualizer::ProcessLine(const QString& line)
{
    QStringList cells = line.split("-");

    if (cells.size() != 2)
        return QSharedPointer<MazeAnimationData::Edge>();

    QSharedPointer<MazeAnimationData::Cell> from = ProcessCell(cells[0]);
    QSharedPointer<MazeAnimationData::Cell> to = ProcessCell(cells[1]);

    if (from.isNull() || to.isNull())
        return QSharedPointer<MazeAnimationData::Edge>();

    return QSharedPointer<MazeAnimationData::Edge>(new MazeAnimationData::Edge(from, to));
}

// rows and colls info is formated in line as "rows,cols"
void MazeVisualizer::ParseSizeInfo(const QString& line, unsigned int& rows, unsigned int& cols)
{
    rows = 0;
    cols = 0;

    QStringList tokens = line.split(",");

    if (tokens.size() != 2)
        return;

    rows = tokens[0].toInt();
    cols = tokens[1].toInt();

    if ( ((int) rows) < 0)
        rows = 0;

    if ( ((int) cols) < 0)
        rows = 0;
}

QSharedPointer<MazeAnimationData> MazeVisualizer::ParseInputFile(const QString& filename)
{
    // open data file
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return QSharedPointer<MazeAnimationData>();

    // setup stream reading
    QTextStream in(&file);
    QString line;

    // read rows and cols info (1st line)
    if (!in.atEnd())
        line = in.readLine();

    unsigned int rows = 0;
    unsigned int cols = 0;
    ParseSizeInfo(line, rows, cols);

    if (rows == 0 || cols == 0)
        return QSharedPointer<MazeAnimationData>();

    // read all edges
    QVector<QSharedPointer<MazeAnimationData::Edge>> edges;
    while (!in.atEnd()) {
        line = in.readLine();

        int progress = 100.0 / ((rows * cols) - (rows - cols)) * edges.size();
        emit ProgressUpdated(progress);

        QSharedPointer<MazeAnimationData::Edge> edge = ProcessLine(line);
        if (edge.isNull())
            return QSharedPointer<MazeAnimationData>();

        edges.push_back(edge);
    }

    file.close();

    return QSharedPointer<MazeAnimationData>(new MazeAnimationData(rows, cols, edges));
}
