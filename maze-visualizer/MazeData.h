/*
 * Date:    24.09.2016
 * Author:  Jaroslav Petrovsky
 *
 * Brief:   MazeData class holds information about maze visualization process
 */

#ifndef MAZEDATA_H
#define MAZEDATA_H

#include <QVector>
#include <QSharedPointer>

class MazeAnimationData
{
public:
    /*
     * Cell class
     */
    class Cell
    {
    public:
        Cell(unsigned int row, unsigned int col)
            : m_row(row), m_col(col)
        {}
        virtual ~Cell() = default;

        inline unsigned int GetRow() const { return m_row; }
        inline unsigned int GetCol() const { return m_col; }

    private:
        unsigned int m_row;
        unsigned int m_col;
    };

    /*
     * Edge class
     */
    class Edge
    {
    public:
        Edge(QSharedPointer<MazeAnimationData::Cell> from, QSharedPointer<MazeAnimationData::Cell> to)
            : m_fromCell(from), m_toCell(to)
        {}
        virtual ~Edge() = default;

        inline QSharedPointer<MazeAnimationData::Cell> GetFromCell() const { return m_fromCell; }
        inline QSharedPointer<MazeAnimationData::Cell> GetToCell() const   { return m_toCell; }

    private:
        QSharedPointer<MazeAnimationData::Cell> m_fromCell;
        QSharedPointer<MazeAnimationData::Cell> m_toCell;
    };

    /*
     * MazeData class
     */
    MazeAnimationData(
            unsigned int rowsNumber,
            unsigned int colsNumber,
            const QVector<QSharedPointer<MazeAnimationData::Edge>>& edges)
        : m_rowsNumber(rowsNumber), m_colsNumber(colsNumber), m_edges(edges)
    {}
    ~MazeAnimationData() = default;

    inline unsigned int GetRowsNumber() const { return m_rowsNumber; }
    inline unsigned int GetColsNumber() const { return m_colsNumber; }
    inline QVector<QSharedPointer<MazeAnimationData::Edge>> GetEdges() const   { return m_edges; }

private:
    unsigned int m_rowsNumber;
    unsigned int m_colsNumber;
    QVector<QSharedPointer<MazeAnimationData::Edge>> m_edges;

};

#endif // MAZEDATA_H
