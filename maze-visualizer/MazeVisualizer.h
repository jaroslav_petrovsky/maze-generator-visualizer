/*
 * Date:    19.09.2016
 * Author:  Jaroslav Petrovsky
 *
 * Brief:   MazeVisualizer is application object which intializes main window
 * in which all events and processing happens.
 */

#ifndef MAZEVISUALIZER_H
#define MAZEVISUALIZER_H

#include "MazeData.h"

#include <QString>
#include <QSharedPointer>

class MainWindow;
class MazeAnimationData;

class MazeVisualizer : public QObject
{
    Q_OBJECT

public:
    MazeVisualizer();
    ~MazeVisualizer() = default;

    // return true if input file was parsed successfuly
    inline bool IsOk() { return m_isOK; }

signals:
    void ProgressUpdated(int progress);

private:
    QSharedPointer<MazeAnimationData> ParseInputFile(const QString& filename);
    void ParseSizeInfo(const QString& line, unsigned int& rows, unsigned int& cols);
    QSharedPointer<MazeAnimationData::Edge> ProcessLine(const QString& line);
    QSharedPointer<MazeAnimationData::Cell> ProcessCell(const QString& cellInfo);

private:
    QSharedPointer<MainWindow> m_mainWindow;
    QSharedPointer<MazeAnimationData> m_mazeAnimationData;

    bool m_isOK;
};

#endif // MAZEVISUALIZER_H
