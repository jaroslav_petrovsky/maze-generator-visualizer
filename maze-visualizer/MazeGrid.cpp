/*
 * Date:    26.09.2016
 * Author:  Jaroslav Petrovsky
 *
 * Brief:   Widget which displays maze grid animation.
 */

#include "MazeGrid.h"
#include "MazeData.h"

#include <QPainter>
#include <QtGlobal>
#include <QMessageBox>
#include <QMutexLocker>
#include <QApplication>
#include <QDesktopWidget>

#define PLAY_INTERVAL   10
#define PLAY_STEP       10

MazeGrid::MazeGrid(QWidget* parentW) :
    QWidget(parentW),
    m_mazeAnimationData(nullptr),
    m_cells(),
    m_edges(),
    m_drawingMutex(),
    m_isPlaying(false),
    m_playTimer(),
    m_minCellSize(6),
    m_currentCellSize(0),
    m_edgeSize(3),
    m_borderSize(5),
    m_margin(10),
    m_originX(0),
    m_originY(0),
    m_mazeWidth(0),
    m_mazeHeight(0),
    m_hiddenCellColor("gray"),
    m_visibleCellColor("white"),
    m_animatingCellColor(m_hiddenCellColor),
    m_edgeColor("black"),
    m_borderColor("black"),
    m_playSpeed(),
    m_progress(0),
    m_edgeIndex(0),
    m_animationStep(0),
    m_currentFromCell(),
    m_currentToCell()
{
    // calls timerEvent() in given interval in milliseconds
    this->startTimer(100);

    QObject::connect(&this->m_playTimer, SIGNAL(timeout()), this, SLOT(Animate()));
}

void MazeGrid::SetMazeAnimationData(QSharedPointer<MazeAnimationData> mazeAnimationData)
{
    QMutexLocker(&this->m_drawingMutex);

    m_mazeAnimationData = mazeAnimationData;

    InitMazeGridData();
    ComputeDimensions();
}

void MazeGrid::ShowMaze()
{
    this->ChangeMazeState(MazeGrid::DrawingState::VISIBLE);
}

void MazeGrid::ResetMaze()
{
    this->ChangeMazeState(MazeGrid::DrawingState::HIDDEN);
}

void MazeGrid::PlayButtonClicked(bool checked)
{
    QMutexLocker(&this->m_drawingMutex);

    if (checked == true) {
        // continue playing
        m_playTimer.setInterval(PLAY_INTERVAL / m_playSpeed / PLAY_STEP);
        m_playTimer.start();

    } else if (checked == false){
        // pause playing
        m_playTimer.stop();
    }
}

void MazeGrid::SetPlaySpeed(int playSpeed)
{
    QMutexLocker(&this->m_drawingMutex);

    m_playSpeed = playSpeed;

    m_playTimer.setInterval(PLAY_INTERVAL / m_playSpeed / PLAY_STEP);
}

void MazeGrid::paintEvent(QPaintEvent* pEvent)
{
    QWidget::paintEvent(pEvent);

    // do not display maze if dimension were not computed yet
    if (m_mazeWidth == 0 && m_mazeHeight == 0)
        return;

    QMutexLocker(&this->m_drawingMutex);

    QPainter painter(this);

    // has to be in this order to display maze correctly
    DrawCells(painter);
    DrawEdges(painter);
    DrawBorder(painter);
}

void MazeGrid::timerEvent(QTimerEvent* tEvent)
{
    QWidget::timerEvent(tEvent);

    // calls paintEvent()
    this->update();
}

// if called in wrong moment this function can screw up all the data
// m_drawingMutex lock should be acquired before calling
void MazeGrid::InitMazeGridData()
{
    // clear all the data because we have new
    m_cells.clear();
    m_edges.clear();

    InitCells();
    InitEdges();
}

// m_drawingMutex lock should be acquired before calling
void MazeGrid::InitCells()
{
    unsigned int rowsNumber = m_mazeAnimationData->GetRowsNumber();
    unsigned int colsNumber = m_mazeAnimationData->GetColsNumber();

    int edges_number = (rowsNumber * colsNumber) - (rowsNumber + colsNumber);
    int components_number = rowsNumber * colsNumber + edges_number;

    for(unsigned int row = 0; row < rowsNumber; ++row) {
        for(unsigned int col = 0; col < colsNumber; ++col) {
            m_cells.push_back(QSharedPointer<MazeGrid::Cell>(new MazeGrid::Cell(
                row,
                col,
                QSharedPointer<MazeGrid::Edge>(nullptr), // InitEdges() will setup
                QSharedPointer<MazeGrid::Edge>(nullptr), // InitEdges() will setup
                MazeGrid::DrawingState::HIDDEN)));

            int current_progress = 100.0 / components_number * m_cells.size();

            if (m_progress != current_progress) {
                m_progress = current_progress;
                emit ProgressUpdated(m_progress);
            }
        }
    }
}

// m_drawingMutex lock should be acquired before calling
void MazeGrid::InitEdges()
{
    unsigned int rowsNumber = m_mazeAnimationData->GetRowsNumber();
    unsigned int colsNumber = m_mazeAnimationData->GetColsNumber();

    int edges_number = (rowsNumber * colsNumber) - (rowsNumber + colsNumber);
    int components_number = rowsNumber * colsNumber + edges_number;

    for(unsigned int row = 0; row < rowsNumber; ++row) {
        for(unsigned int col = 0; col < colsNumber; ++col) {
            QSharedPointer<MazeGrid::Cell> fromCell = this->GetCell(row, col);
            QSharedPointer<MazeGrid::Cell> toCell = QSharedPointer<MazeGrid::Cell>(nullptr);

            if (col > 0) {
                toCell = this->GetCell(row, col - 1);

                QSharedPointer<MazeGrid::Edge> westEdge = this->CreateEdge(fromCell, toCell);

                fromCell->SetWestEdge(westEdge);
                m_edges.push_back(westEdge);
            }

            if ( row > 0 ) {
                toCell = this->GetCell(row - 1, col);

                QSharedPointer<MazeGrid::Edge> northEdge = this->CreateEdge(fromCell, toCell);

                fromCell->SetNorthEdge(northEdge);
                m_edges.push_back(northEdge);
            }

            int current_progress = 100.0 / components_number * (m_cells.size() + m_edges.size());

            if (m_progress != current_progress) {
                m_progress = current_progress;
                emit ProgressUpdated(m_progress);
            }
        }
    }
}

// m_drawingMutex lock should be acquired before calling
QSharedPointer<MazeGrid::Cell> MazeGrid::GetCell(unsigned int row, unsigned int col)
{
    return m_cells[this->GetCellIndex(row,col)];
}

// m_drawingMutex lock should be acquired before calling
QSharedPointer<MazeGrid::Edge> MazeGrid::CreateEdge(
        QSharedPointer<MazeGrid::Cell> fromCell,
        QSharedPointer<MazeGrid::Cell> toCell)
{
    if (fromCell.isNull() || toCell.isNull())
        return QSharedPointer<MazeGrid::Edge>(nullptr);

    return QSharedPointer<MazeGrid::Edge>(
                new MazeGrid::Edge(fromCell, toCell, MazeGrid::DrawingState::VISIBLE));
}

// m_drawingMutex lock should be acquired before calling
unsigned int MazeGrid::GetCellIndex(unsigned int row, unsigned int col)
{
    return (row * m_mazeAnimationData->GetColsNumber() + col);
}

// m_drawingMutex lock should be acquired before calling
void MazeGrid::ComputeDimensions()
{
    int widgetWidth = this->width();
    int widgetHeight = this->height();
    int rowsNumber = m_mazeAnimationData->GetRowsNumber();
    int colsNumner = m_mazeAnimationData->GetColsNumber();

    // find out largest possible cell size according to width and height
    size_t cellWidthSize = (widgetWidth - 2 * m_margin) / colsNumner;

    size_t cellHeightSize = (widgetHeight - 2 * m_margin) / rowsNumber;

    size_t smallerSize = qMin(cellWidthSize, cellHeightSize);

    // if smaller than minimum cell size, inform user that output is bigger than
    // default window sizee
    if (smallerSize < m_minCellSize) {
        m_currentCellSize = m_minCellSize;

        QMessageBox messageBox;

        if (CanResizeToFit() == true) {
            QMessageBox::StandardButton reply = messageBox.information(
                        this,
                        "Maze too big",
                        "Maze is too big to display whole! Resize to fit?",
                        QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);

            if (reply == QMessageBox::Yes) {
                // update
                ResizeToFit();

                widgetWidth = this->width();
                widgetHeight = this->height();
            }
        } else {
            messageBox.information(
                        this,
                        "Maze too big",
                        "Maze is too big to display whole!");
        }
    } else {
        m_currentCellSize = smallerSize;
    }

    // compute x and y coors which will be used as origin
    m_mazeWidth = colsNumner * m_currentCellSize;
    m_originX = widgetWidth / 2 - m_mazeWidth / 2;

    m_mazeHeight = rowsNumber * m_currentCellSize;
    m_originY = widgetHeight / 2 - m_mazeHeight / 2;
}

// m_drawingMutex lock should be acquired before calling
void MazeGrid::DrawEdges(QPainter& painter)
{
    for (QSharedPointer<MazeGrid::Edge> edge : m_edges) {
        // use cells to determine which edge (north/west) and compute edge coordinates
        QSharedPointer<MazeGrid::Cell> fromCell =
                qSharedPointerCast<MazeGrid::Cell>(edge->GetFromCell());
        QSharedPointer<MazeGrid::Cell> toCell =
                qSharedPointerCast<MazeGrid::Cell>(edge->GetToCell());

        size_t fromRow = fromCell->GetRow();
        size_t fromCol = fromCell->GetCol();
        size_t toRow = toCell->GetRow();
        size_t toCol = toCell->GetCol();

        size_t fromX = m_originX + (fromCol * m_currentCellSize);
        size_t fromY = m_originY + (fromRow * m_currentCellSize);
        size_t toX = 0;
        size_t toY = 0;

        if (fromRow != toRow) {
            // north
            toX = fromX + m_currentCellSize;
            toY = fromY;
        } else if (fromCol != toCol) {
            // west
            toX = fromX;
            toY = fromY + m_currentCellSize;
        }

        DrawEdge(fromX, fromY, toX, toY, edge->GetDrawingState(), painter);
    }
}

// m_drawingMutex lock should be acquired before calling
void MazeGrid::DrawCells(QPainter& painter)
{
    for (QSharedPointer<MazeGrid::Cell> cell : m_cells) {
        size_t row = cell->GetRow();
        size_t col = cell->GetCol();

        size_t fromX = m_originX + (col * m_currentCellSize);
        size_t fromY = m_originY + (row * m_currentCellSize);

        DrawCell(fromX,
                    fromY,
                    m_currentCellSize,
                    m_currentCellSize,
                    cell->GetDrawingState(),
                    painter);
    }
}

// m_drawingMutex lock should be acquired before calling
void MazeGrid::DrawBorder(QPainter& painter)
{
    // save&restore state so painter will remain unchanged to caller
    painter.save();

    QPen pen(m_borderColor, m_borderSize, Qt::SolidLine);

    painter.setPen(pen);

    painter.drawLine(m_originX, m_originY, m_originX + m_mazeWidth, m_originY);
    painter.drawLine(m_originX + m_mazeWidth, m_originY, m_originX + m_mazeWidth, m_originY + m_mazeHeight);
    painter.drawLine(m_originX + m_mazeWidth, m_originY + m_mazeHeight, m_originX, m_originY + m_mazeHeight);
    painter.drawLine(m_originX, m_originY + m_mazeHeight, m_originX, m_originY);

    painter.restore();
}

// m_drawingMutex lock should be acquired before calling
void MazeGrid::DrawEdge(
        size_t fromX,
        size_t fromY,
        size_t toX,
        size_t toY,
        MazeGrid::DrawingState drawingState,
        QPainter& painter)
{
    // save&restore state so painter will remain unchanged to caller
    painter.save();

    QColor edgeColor = m_edgeColor;

    if (drawingState == MazeGrid::DrawingState::HIDDEN)
        edgeColor.setAlpha(0);

    QPen pen(edgeColor, m_edgeSize, Qt::SolidLine);

    painter.setPen(pen);

    painter.drawLine(fromX, fromY, toX, toY);

    painter.restore();
}

// m_drawingMutex lock should be acquired before calling
void MazeGrid::DrawCell(
        size_t fromX,
        size_t fromY,
        size_t cellWidth,
        size_t cellHeight,
        MazeGrid::DrawingState drawingState,
        QPainter& painter)
{
    // save&restore state so painter will remain unchanged to caller
    painter.save();

    QColor brushColor;

    switch (drawingState) {
        case MazeGrid::DrawingState::ANIMATING:
            brushColor = m_animatingCellColor;
            break;
        case MazeGrid::DrawingState::VISIBLE:
            brushColor = m_visibleCellColor;
            break;
        case MazeGrid::DrawingState::HIDDEN:
            brushColor = m_hiddenCellColor;
            break;
    }

    QBrush brush(brushColor);

    painter.fillRect(fromX, fromY, cellWidth, cellHeight, brush);

    painter.restore();
}

// m_drawingMutex lock should be acquired before calling
bool MazeGrid::CanResizeToFit()
{
    QRect rec = QApplication::desktop()->screenGeometry();
    int screenHeight = rec.height();
    int screenWidth = rec.width();

    int mazeWidth = m_mazeAnimationData->GetColsNumber() * m_minCellSize;
    int mazeHeight = m_mazeAnimationData->GetRowsNumber() * m_minCellSize;

    int mazeBorderWidth = this->parentWidget()->width() - this->width();
    int mazeBorderHeight = this->parentWidget()->height() - this->height();

    if ( (mazeWidth + mazeBorderWidth <= screenWidth) &&
        (mazeHeight + mazeBorderHeight <= screenHeight) )
            return true;

    return false;
}

// m_drawingMutex lock should be acquired before calling
void MazeGrid::ResizeToFit()
{
    int mazeWidth = m_mazeAnimationData->GetColsNumber() * m_minCellSize;
    int mazeHeight = m_mazeAnimationData->GetRowsNumber() * m_minCellSize;

    this->setFixedSize(mazeWidth + m_margin, mazeHeight + m_margin);
}

// utility function for changing/reseting maze state
void MazeGrid::ChangeMazeState(MazeGrid::DrawingState state)
{
    QMutexLocker(&this->m_drawingMutex);

    MazeGrid::DrawingState cellState;
    MazeGrid::DrawingState edgeState;

    if (state == MazeGrid::DrawingState::VISIBLE) {
        cellState = MazeGrid::DrawingState::VISIBLE;
        edgeState = MazeGrid::DrawingState::HIDDEN;
    } else if (state == MazeGrid::DrawingState::HIDDEN) {
        cellState = MazeGrid::DrawingState::HIDDEN;
        edgeState = MazeGrid::DrawingState::VISIBLE;
    }

    m_edgeIndex = 0;
    for (QSharedPointer<MazeAnimationData::Edge> edge : m_mazeAnimationData->GetEdges())
    {
        m_edgeIndex++;

        QSharedPointer<MazeGrid::Cell> fromCell =
                GetCell(edge->GetFromCell()->GetRow(), edge->GetFromCell()->GetCol());
        QSharedPointer<MazeGrid::Cell> toCell =
                GetCell(edge->GetToCell()->GetRow(), edge->GetToCell()->GetCol());

        // set cells to visible
        fromCell->SetDrawingState(cellState);
        toCell->SetDrawingState(cellState);

        // find edge
        if (fromCell->GetCol() == toCell->GetCol())
        {
            // north edge
            fromCell->GetNorthEdge()->SetDrawingState(edgeState);
        } else if (fromCell->GetRow() == toCell->GetRow()) {
            // west edge
            fromCell->GetWestEdge()->SetDrawingState(edgeState);
        }

        int current_progress = 100.0 / m_mazeAnimationData->GetEdges().size() * m_edgeIndex;

        if (m_progress != current_progress) {
            m_progress = current_progress;
            emit ProgressUpdated(m_progress);
        }
    }

    // reset animation
    m_edgeIndex = 0;
    m_animationStep = PLAY_STEP - 1;
}

void MazeGrid::Animate()
{
    QMutexLocker(&this->m_drawingMutex);

    if (--m_animationStep < 0) {
        // pick next edge
        m_animationStep = PLAY_STEP - 2;

        // finish animating previous cells
        if (!m_currentFromCell.isNull())
            m_currentFromCell->SetDrawingState(MazeGrid::DrawingState::VISIBLE);
        if (!m_currentToCell.isNull())
            m_currentToCell->SetDrawingState(MazeGrid::DrawingState::VISIBLE);

        QSharedPointer<MazeAnimationData::Edge> edge =
                m_mazeAnimationData->GetEdges()[m_edgeIndex++];

        m_currentFromCell =
                GetCell(edge->GetFromCell()->GetRow(), edge->GetFromCell()->GetCol());
        m_currentToCell =
                GetCell(edge->GetToCell()->GetRow(), edge->GetToCell()->GetCol());

        // set cells to visible
        m_currentFromCell->SetDrawingState(MazeGrid::DrawingState::ANIMATING);
        m_currentToCell->SetDrawingState(MazeGrid::DrawingState::ANIMATING);

        // find edge
        if (m_currentFromCell->GetCol() == m_currentToCell->GetCol())
        {
            // north edge
            m_currentFromCell->GetNorthEdge()->SetDrawingState(MazeGrid::DrawingState::HIDDEN);
        } else if (m_currentFromCell->GetRow() == m_currentToCell->GetRow()) {
            // west edge
            m_currentFromCell->GetWestEdge()->SetDrawingState(MazeGrid::DrawingState::HIDDEN);
        }

    }

    int current_progress = 100.0 / m_mazeAnimationData->GetEdges().size() * m_edgeIndex;

    if (m_progress != current_progress) {
        m_progress = current_progress;
        emit ProgressUpdated(m_progress);
    }

    if (m_edgeIndex >= m_mazeAnimationData->GetEdges().size()) {
        // finish animating
        m_playTimer.stop();

        m_currentFromCell->SetDrawingState(MazeGrid::DrawingState::VISIBLE);
        m_currentToCell->SetDrawingState(MazeGrid::DrawingState::VISIBLE);

        // reset animation
        m_edgeIndex = 0;
        m_animationStep = PLAY_STEP - 1;

        emit AnimationDone();

        return;
    }

    m_animatingCellColor.setAlpha(255 / (PLAY_STEP - 1) * m_animationStep);
}
