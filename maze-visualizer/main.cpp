/*
 * Date:    19.09.2016
 * Author:  Jaroslav Petrovsky
 *
 * Brief:   main function
 */

#include "MazeVisualizer.h"

#include <QApplication>

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    MazeVisualizer* visualizer = new MazeVisualizer();

    if (visualizer->IsOk())
        return app.exec();
    else
        return 0;
}

